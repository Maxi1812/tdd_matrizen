﻿ using System;
namespace Matrizen_TDD.models
{
    public class Matrix
    {

        public int Column { get; set; }
        public int Row { get; set; }
        private double[,] _matrix;

        public double this[int row, int col]
        {
            get
            {
                if ((col >= 0) && (row >= 0) && (row < this.Row) && (col < this.Column))
                {
                    return this._matrix[row, col];
                }
                else
                {
                    throw new ArgumentOutOfRangeException();
                }
            }
            set
            {
                if ((col >= 0) && (row >= 0) && (row < this.Row) && (col < this.Column))
                {
                    _matrix[row, col] = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException();
                }
            }
        }

        public Matrix() : this(0, 0){}
        public Matrix(int row, int col) {

            this.Column = col;
            this.Row = row;
            this._matrix = new double[this.Row, this.Column];
        }

        public Matrix Add(Matrix m1, Matrix m2) 
        {
            Matrix mErg = new Matrix(m1.Row, m1.Column);
            if (m1.Column == m2.Column && m1.Row == m2.Row)
            {
                for (int r = 0; r < m1.Row; r++)
                {
                    for (int c = 0; c < m1.Column; c++)
                    {
                        mErg[r, c] = m1[r, c] + m2[r, c];
                    }
                }
                return mErg;
            }
            else
            {
                throw new ArgumentOutOfRangeException();
            }
        }

        public Matrix Substract(Matrix m1, Matrix m2)
        {
            Matrix mErg = new Matrix(m1.Row, m1.Column);
            if (m1.Column == m2.Column && m1.Row == m2.Row)
            {
                for (int r = 0; r < m1.Row; r++)
                {
                    for (int c = 0; c < m1.Column; c++)
                    {
                        mErg[r, c] = m1[r, c] - m2[r, c];
                    }
                }
                return mErg;
            }
            else
            {
                throw new ArgumentOutOfRangeException();
            }
        }
        public Matrix SkalarMultiplication(Matrix m, double number) 
        {
            Matrix mErg = new Matrix(m.Row, m.Column);
            if (m.Column > 1)
            {
                throw new ArgumentOutOfRangeException();
            }
            else
            {
                for (int r = 0; r < m.Row; r++)
                {
                    for (int c = 0; c < m.Column; c++)
                    {
                        mErg[r, c] = m[r, c] * number;
                    }   
                }
                return mErg;
            }
        }
        static public Matrix operator +(Matrix z1, Matrix z2)
        {
             return z1.Add(z1, z2);
        }
        static public Matrix operator -(Matrix z1, Matrix z2)
        {
            return z1.Substract(z1, z2);
        }

        public Matrix Multiply(Matrix m1, Matrix m2)
        {
            Matrix mErg = new Matrix(m1.Row, m2.Column);
            if (m1.Column != m2.Row)
            {
                throw new ArgumentOutOfRangeException();
            }
            else
            {
                for (int r = 0; r < m2.Row; r++)
                {
                    for (int c = 0; c < m2.Column; c++)
                    {
                        for (int i = 0; i < m1.Row; i++)
                        {
                            mErg[i, c] = m1[i, r] * m2[r, c];
                        }

                    }
                }
                return mErg;
            }
        }

        public Matrix TransposedMatrix(Matrix m) 
        {
            Matrix mErg = new Matrix(m.Column, m.Row);
            for (int r = 0; r < m.Row; r++)
            {
                for (int c = 0; c < m.Column; c++)
                {
                    mErg[c, r] = m[c, r];
                }
            }
            return mErg;
        }
    }
}
