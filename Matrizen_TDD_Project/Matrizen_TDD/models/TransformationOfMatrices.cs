﻿using System;
namespace Matrizen_TDD.models
{
    public class TransformationOfMatrices
    {
        public Matrix Translation(double translationX, double translationY, double translationZ) 
        {
            Matrix result = new Matrix(4, 4);
            for (int r = 0; r < 4; r++)
            {
                for (int c = 0; c < 4; c++)
                {
                    if (r == c)
                    {
                        result[r, c] = 1;
                    }
                    else
                    {
                        result[r, c] = 0;
                    }
                }
            }
            result[0, 3] = translationX;
            result[1, 3] = translationY;
            result[2, 3] = translationZ;
            return result;
        }
    }
}
