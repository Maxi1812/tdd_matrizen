using System;
using NUnit.Framework;
using Matrizen_TDD.models;
using System.Collections.Generic;

namespace Matrizen_TDD.Test.models
{
    [TestFixture]
    class MatrixTest
    {
        [Test]
        public void StdCtorShouldSetRowsAndColumnsToZero()
        {

            // Arrange

            //Act
            Matrix m = new Matrix();

            //Assert
            Assert.That(0, Is.EqualTo(m.Column));
            Assert.That(0, Is.EqualTo(m.Row));
        }
        [TestCase(3, 4)]
        public void CtorShouldSetRowsAndColumnsCorrectly(int row, int col)
        {
            //Arrange
            //Act
            Matrix z = new Matrix(row, col);
            //Assert
            Assert.That(col, Is.EqualTo(z.Column));
            Assert.That(row, Is.EqualTo(z.Row));
            for (int zeile = 0; zeile < z.Row; zeile++)
            {
                for (int spalte = 0; spalte < z.Column; spalte++)
                {
                    Assert.That(0.0, Is.EqualTo(z[zeile, spalte]));
                }
            }
        }
        [TestCase(-3, 4)]
        [TestCase(3, -4)]
        [TestCase(-3, -4)]
        public void IndexersShouldThrowExcepetionIfRowOrColoumnOutOfRange(int row, int col)
        {
            //Arrange
            //Act
            Matrix z = new Matrix(5, 5);
            //Assert
            Assert.Throws<ArgumentOutOfRangeException>(() => { z[row, col] = -3; });
        }
        [TestCase(3, 4, 3, 4)]
        public void MatricesShouldHaveSameRowsAndColumns(int row1, int col1, int row2, int col2)
        {
            //Arrange
            Matrix m1 = new Matrix(row1, col1);
            Matrix m2 = new Matrix(row2, col2);
            //Act
            //Assert 
            Assert.That(col1, Is.EqualTo(col2));
            Assert.That(row1, Is.EqualTo(row2));
        }
        [TestCase(1, 3, new double[3] { 1, 2, 3 }, new double[3] { 1, 2, 3 }, new double[3] { 2, 4, 6 })]
        public void AddShouldCalculateMatricesCorrectly(int row, int col, double[] matrix1, double[] matrix2, double[] erg)
        {
            //Arrange
            Matrix m1 = new Matrix(row, col);
            Matrix m2 = new Matrix(row, col);
            Matrix mErg = new Matrix(row, col);
            //Act
            int arrayIndex = 0;
            for (int r = 0; r < row; r++)
            {
                for (int c = 0; c < col; c++)
                {
                    m1[r, c] = matrix1[arrayIndex];
                    m2[r, c] = matrix2[arrayIndex];
                    mErg[r, c] = erg[arrayIndex];
                    arrayIndex++;
                }
            }
            Matrix result = m1.Add(m1, m2);
            //Assert
            for (int r = 0; r < row; r++)
            {
                for (int c = 0; c < col; c++)
                {
                    Assert.That(mErg[r, c], Is.EqualTo(result[r, c]));
                }
            }
        }
        [TestCase(3, 3, new double[] { 1, 2, 3, 4 ,5, 6, 7, 8, 9 }, new double[] { 1, 2, 3 ,4,5,6,7,8,9 }, new double[] {0,0,0,0,0,0,0,0,0})]
        [TestCase(1, 3, new double[3] { 1, 2, 3 }, new double[3] { 1, 2, 3 }, new double[3] { 0, 0, 0 })]
        [TestCase(1, 3, new double[3] { 1, 2, 3 }, new double[3] { 1, 2, 3 }, new double[3] { 0, 0, 0 })]

        public void SubstractShouldCalculateMatricesCorrectly(int row, int col, double[] matrix1, double[] matrix2, double[] erg)
        {
            //Arrange
            Matrix m1 = new Matrix(row, col);
            Matrix m2 = new Matrix(row, col);
            Matrix mErg = new Matrix(row, col);
            //Act
            int arrayIndex = 0;
            for (int r = 0; r < row; r++)
            {
                for (int c = 0; c < col; c++)
                {
                    m1[r, c] = matrix1[arrayIndex];
                    m2[r, c] = matrix2[arrayIndex];
                    mErg[r, c] = erg[arrayIndex];
                    arrayIndex++;
                }
            }
            Matrix result = m1.Substract(m1, m2);
            //Assert
            for (int r = 0; r < row; r++)
            {
                for (int c = 0; c < col; c++)
                {
                    Assert.That(mErg[r, c], Is.EqualTo(result[r, c]));
                }
            }
        }
        [TestCase(5, 2)]
        public void SkalarMultiplicationShouldThrowExpetionIfRowGreatherThan1(int row, int col) 
        {
            //Arrange
            //Act
            Matrix z = new Matrix(5, 1);
            //Assert
            Assert.Throws<ArgumentOutOfRangeException>(() => { z[row, col] = -3; });

        }

        [TestCase(3, 1, new double[] { 1, 2, 3 }, 2, new double[] { 2, 4, 6 })]
        public void SkalarMulitplicationShouldCalculateMarticesCorrectly(int row, int col, double[] matrix, int number, double[] erg)
        {
            //Arrange
            Matrix m1 = new Matrix(row, col);
            Matrix mErg = new Matrix(row, col);

            //Act
            int arrayIndex = 0;
            for (int r = 0; r < row; r++)
            {
                for (int c = 0; c < col; c++)
                {
                    m1[r, c] = matrix[arrayIndex];
                    mErg[r, c] = erg[arrayIndex];
                    arrayIndex++;
                }
            }
            Matrix result = m1.SkalarMultiplication(m1, number);
            //Assert
            for (int r = 0; r < row; r++)
            {
                for (int c = 0; c < col; c++)
                {
                    Assert.That(mErg[r, c], Is.EqualTo(result[r, c]));
                }
            }
        }


        [TestCase(1, 3, new double[3] { 1, 2, 3 }, new double[3] { 1, 2, 3 }, new double[3] { 2, 4, 6 })]
        public void AddOperatorShouldCalculateMatricesCorrectly(int row, int col, double[] matrix1, double[] matrix2, double[] erg)
        {
            //Arrange
            Matrix m1 = new Matrix(row, col);
            Matrix m2 = new Matrix(row, col);
            Matrix mErg = new Matrix(row, col);
            //Act
            int arrayIndex = 0;
            for (int r = 0; r < row; r++)
            {
                for (int c = 0; c < col; c++)
                {
                    m1[r, c] = matrix1[arrayIndex];
                    m2[r, c] = matrix2[arrayIndex];
                    mErg[r, c] = erg[arrayIndex];
                    arrayIndex++;
                }
            }
            Matrix result = m1 + m2;
            //Assert
            for (int r = 0; r < row; r++)
            {
                for (int c = 0; c < col; c++)
                {
                    Assert.That(mErg[r, c], Is.EqualTo(result[r, c]));
                }
            }
        }
        [TestCase(1, 3, new double[3] { 1, 2, 3 }, new double[3] { 1, 2, 3 }, new double[3] { 0, 0, 0 })]
        public void SubstractOperatorShouldCalculateMatricesCorrectly(int row, int col, double[] matrix1, double[] matrix2, double[] erg)
        {
            //Arrange
            Matrix m1 = new Matrix(row, col);
            Matrix m2 = new Matrix(row, col);
            Matrix mErg = new Matrix(row, col);
            //Act
            int arrayIndex = 0;
            for (int r = 0; r < row; r++)
            {
                for (int c = 0; c < col; c++)
                {
                    m1[r, c] = matrix1[arrayIndex];
                    m2[r, c] = matrix2[arrayIndex];
                    mErg[r, c] = erg[arrayIndex];
                    arrayIndex++;
                }
            }
            Matrix result = m1 - m2;
            //Assert
            for (int r = 0; r < row; r++)
            {
                for (int c = 0; c < col; c++)
                {
                    Assert.That(mErg[r, c], Is.EqualTo(result[r, c]));
                }
            }
        }
        [TestCase(3,3 ,3,2, new double[] { 1, 2, 3,4,5,6,7,8,9 }, new double[] { 1, 2, 3,4,5,6 }, new double[] {22,28,49,64,76,100 })]
        [TestCase(1, 4, 4, 3, new double[] { -1,-2,-3,-4 }, new double[] { 1, 2, 3, 4, 5, 6,7,8,9,10,11,12 }, new double[] { -70, -80, -90 })]
        public void MulitplicationShouldCalculateMarticesCorrectly(int row1, int col1,int row2,int col2, double[] matrix1,double[] matrix2 , double[] erg)
        {
            //Arrange
            Matrix m1 = new Matrix(row1, col1);
            Matrix m2 = new Matrix(row2, col2);
            Matrix mErg = new Matrix(row1, col2);

            //Act
            int arrayIndex1 = 0;
            for (int r = 0; r < row1; r++)
            {
                for (int c = 0; c < col1; c++)
                {
                    m1[r, c] = matrix1[arrayIndex1];
                    arrayIndex1++;
                }
            }
            int arrayIndex2 = 0;
            for (int r = 0; r < row1; r++)
            {
                for (int c = 0; c < col1; c++)
                {
                    m1[r, c] = matrix1[arrayIndex2];
                    arrayIndex2++;
                }
            }
            Matrix result = m1.Multiply(m1, m2);
            //Assert
            for (int r = 0; r < row1; r++)
            {
                for (int c = 0; c < col2; c++)
                {
                    Assert.That(mErg[r, c], Is.EqualTo(result[r, c]));
                }
            }
        }

        [TestCase(2, 2, 2, 2, new double[] { 2, 3, 4, 5 }, new double[] { 2,4,3,5 })]
        [TestCase(2, 2, 2, 2, new double[] { 3, 4, 5, 6 }, new double[] { 3, 5, 4, 6 })]
        public void TransposedMatrixShouldTransposeCorrectly(int row, int col, int row2, int col2, double[] matrix1, double[] erg) 
        {
            //Arrange
            Matrix m1 = new Matrix(row, col);
            Matrix mTransponed = new Matrix(row2, col2);

            //Act
            int arrayIndex = 0;
            for (int r = 0; r < row; r++)
            {
                for (int c = 0; c < col; c++)
                {
                    m1[r, c] = matrix1[arrayIndex];
                    mTransponed[c, r] = erg[arrayIndex];
                    arrayIndex++;
                }
            }
            Matrix result = m1.TransposedMatrix(m1);
            //Assert
            for (int r = 0; r < row; r++)
            {
                for (int c = 0; c < col; c++)
                {
                    Assert.That(mTransponed[c, r], Is.EqualTo(result[c, r]));
                }
            }
        }
    }
}
